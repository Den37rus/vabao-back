const config = require('../config')
const { createServer } = require('http')
const { Server } = require('socket.io')
const app = require('../ExpressServer')

const httpServer = createServer(app)
const socketServer = new Server(httpServer, {
  path: config.SOCKET_PATH,
  transports: ['websocket']
})

const { userConnect, userDisconnect } = require('./userConnectionHandler')

socketServer.on('connection', async socket => {
  const { user } = socket.handshake.auth
  userConnect(user)
  // const user_test = await db.User.findOne({
  //   where: {
  //     id: user
  //   }
  // })
  // console.log(user_test)

  socket.on('disconnect', () => {
    userDisconnect(user)
  })

  socket.on('new_message', message => {
    console.log('new message: ' + message + '  id: ' + user)
    setTimeout(() => {
      socket.emit('message_recive', 'recived from server')
    }, 1000)
  })
})

module.exports = { server: httpServer, io: socketServer }
