const { io } = require('./socket')

// console.log(io)
const infoMessage = (order, message) => {
  io.to(order.id).broadcast({
    id: message.id,
    type: 'info',
    order: {
      id: order.id,
      status: order.Deal.status
    },
    date: new Date(message.date).toLocaleTimeString('ru-RU', {
      timeStyle: 'short'
    })
  })
}

module.exports = infoMessage
