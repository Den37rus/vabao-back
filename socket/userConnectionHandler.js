const db = require('../db')

const userConnect = async id => {
  await db.User.update(
    { lastVisit: Date.now(), isOnline: true },
    {
      where: {
        id: id
      }
    }
  )
}

const userDisconnect = async id => {
  await db.User.update(
    { lastVisit: Date.now(), isOnline: false },
    {
      where: {
        id: id
      }
    }
  )
}

module.exports = { userConnect, userDisconnect }
