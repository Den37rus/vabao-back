const jwt = require('jsonwebtoken')
const config = require('../config')

class TokenFactory {
  #refreshSecret = config.refreshSecret
  #accessSecret = config.accessSecret

  constructor(tokenData) {
    this.refresh = this.#createToken(tokenData, this.#refreshSecret, '30d')
    this.access = this.#createToken(tokenData, this.#accessSecret, '2s')
  }

  #createToken(data, secret, expires) {
    return jwt.sign(data, secret, { expiresIn: expires })
  }
}

class TokenValidator {
  constructor(refreshToken, accessToken) {
    this.refreshToken = refreshToken
    this.accessToken = accessToken
    this.decodedData = this.decode()
    this.isInvalid = !this.validate()
  }

  #refreshSecret = config.refreshSecret
  #accessSecret = config.accessSecret

  verifyToken(token, isRefreshToken = false) {
    try {
      jwt.verify(
        token,
        isRefreshToken ? this.#refreshSecret : this.#accessSecret
      )
      return true
    } catch (e) {
      return false
    }
  }

  validate() {
    this.isRefreshValid = this.verifyToken(this.refreshToken, true)
    this.isAccessValid = this.verifyToken(this.accessToken)
    return this.isRefreshValid && this.isAccessValid
  }

  decode() {
    try {
      return jwt.decode(this.refreshToken)
    } catch (e) {
      return null
    }
  }
}

module.exports = {
  TokenValidator,
  TokenFactory
}
