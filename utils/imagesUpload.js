const path = require('path')
const fs = require('fs')
const sharp = require('sharp')
const config = require('../config')

const orderImagesPath = (folderName = '') =>
  path.join(config.orderImgFolder, String(folderName))

const createOrderImagesThumb = (file, orderId) => {
  const prefix = 'thumb.'
  sharp(file.data)
    .resize(120, 120, { fit: 'inside' })
    .toFile(
      path.join(config.orderImgFolder, orderId, prefix + file.md5) +
        config.imagesExtension,
      err => {
        if (err) console.log(err)
      }
    )
}

const imgToWebp = (
  buffer,
  filePath = path.join(__dirname),
  filename = 'img' + Math.round(Math.random() * 1e5)
) => {
  if (!buffer) throw new Error('Not exists file data in arguments')
  sharp(buffer).toFile(
    path.join(filePath, filename + config.imagesExtension),
    (err, info) => {
      if (err) {
        console.log(err)
        throw err
      }
    }
  )
  return filename + config.imagesExtension
}

const saveOrderPhotos = (files, orderId) => {
  if (!files) throw Error('Files must be exists')
  const images = []
  const filePath = path.join(config.orderImgFolder, orderId.toString())
  if (Array.isArray(files)) {
    files.forEach(file => {
      images.push(imgToWebp(file.data, filePath, file.md5))
      createOrderImagesThumb(file, orderId.toString())
    })
  } else {
    images.push(imgToWebp(files.data, filePath, files.md5))
    createOrderImagesThumb(files, orderId.toString())
  }
  return images
}

const saveUserPhoto = (filename, buffer) => {
  if (!buffer) throw Error('Not exists file data in arguments')
  const filePath = path.join(
    config.userImgFolder,
    filename + config.imagesExtension
  )
  return sharp(buffer)
    .resize(200, 200, { fit: 'inside' })
    .toFile(filePath, (err, info) => {
      if (err) {
        throw err
      }
      return info
    })
}

const dropUserPhoto = id => {
  const photoPath = path.join(config.userImgFolder, id + config.imagesExtension)
  const isPhotoExists = fs.existsSync(photoPath)
  if (isPhotoExists) {
    try {
      fs.rmSync(photoPath)
    } catch (e) {
      console.log(e)
    }
  }
}

module.exports = {
  createOrderImagesThumb,
  saveUserPhoto,
  saveOrderPhotos,
  dropUserPhoto,
  orderImagesPath
}
