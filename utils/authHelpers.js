const db = require('../db')

const clearAuthMeta = (res, message = 'error') => {
  res
    .status(401)
    .clearCookie('__uuid1')
    .clearCookie('login')
    .set('Authorization', null)
    .json({ error: message })
}

const setAuthMeta = (res, refreshToken, accessToken) => {
  res
    .cookie('__uuid1', refreshToken, {
      sameSite: 'lax',
      secure: false,
      maxAge: 2592000000,
      httpOnly: true
    })
    .cookie('login', 1, {
      sameSite: 'lax',
      secure: false,
      maxAge: 2592000000
    })
    .set('Authorization', accessToken)
}

const authSuccessMetaResponse = (
  res,
  refreshToken,
  accessToken,
  data = { ok: true }
) => {
  return res
    .cookie('__uuid1', refreshToken, {
      sameSite: 'lax',
      secure: false,
      maxAge: 2592000000,
      httpOnly: true
    })
    .cookie('login', 1, {
      sameSite: 'lax',
      secure: false,
      maxAge: 2592000000
    })
    .set('Authorization', accessToken)
    .json(data)
}

const isUserExists = async userId => {
  const user = await db.User.findOne({
    where: { id: userId }
  })
  return !!user
}

module.exports = {
  authSuccessMetaResponse,
  clearAuthMeta,
  setAuthMeta,
  isUserExists
}
