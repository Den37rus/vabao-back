const { TokenValidator, TokenFactory } = require('../utils/tokens')
const {
  clearAuthMeta,
  setAuthMeta,
  isUserExists
} = require('../utils/authHelpers')

module.exports = async (req, res, next) => {
  if (req.cookies.login == 0) {
    return clearAuthMeta(res, 'You not authorized')
  }

  if (!req.cookies.__uuid1 || !req.headers.authorization) {
    return clearAuthMeta(res, 'You not authorized')
  }

  const validator = new TokenValidator(
    req.cookies.__uuid1,
    req.headers.authorization
  )

  if (validator.isInvalid) {
    if (validator.isRefreshValid && validator.decodedData) {
      const { refresh, access } = new TokenFactory({
        id: validator.decodedData.id
      })
      req.refreshToken = refresh
      req.accessToken = access
      req.user = validator.decodedData.id
      setAuthMeta(res, refresh, access)
      return next()
    }
    return clearAuthMeta(res, 'You not authorized')
  }

  if (!isUserExists(validator.decodedData.id)) {
    return clearAuthMeta(res, 'Bad userdata')
  }

  req.refreshToken = validator.refreshToken
  req.accessToken = validator.accessToken
  req.user = validator.decodedData.id
  return next()
}
