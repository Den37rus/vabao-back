const router = require('express').Router()
const { body, param, query } = require('express-validator')
const controler = require('../controlers/User.controler.js')
const authMiddleware = require('../midlewares/auth.midleware')

router.get('/:id', param('id').notEmpty().isUUID('4'), controler.getUser)

router.get(
  '/:id/transactions',
  param('id').notEmpty().isUUID('4'),
  authMiddleware,
  controler.getMyTransactions
)

router.post(
  '/:id/payment',
  param('id').notEmpty().isUUID('4'),
  authMiddleware,
  controler.testPayment
)

router.patch('/updateprofile', authMiddleware, controler.updateUserProfile)

module.exports = router
