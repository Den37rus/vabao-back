const router = require('express').Router()
const { body, param, query } = require('express-validator')
const controler = require('../controlers/Orders.controler.js')
const authMiddleware = require('../midlewares/auth.midleware')

router.get('/getPriceRange', controler.getPriceRange)
router.get('/filtered-orders', controler.ordersFiltered)
router.get('/:id', param('id').notEmpty(), controler.getOrder)

router.patch('/:id', authMiddleware, controler.buyOrder)

router.post(
  '/create',
  authMiddleware,
  body('game').isUUID('4'),
  body('category').notEmpty(),
  body('price').notEmpty(),
  controler.createOrder
)

module.exports = router
