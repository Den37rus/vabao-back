const router = require('express').Router()
const { body, param, query } = require('express-validator')
const controler = require('../controlers/Deal.controler.js')
const authMiddleware = require('../midlewares/auth.midleware')

router.get('/', controler.getAllDeals)
router.get(
  '/user/:id',
  param('id').notEmpty().isUUID('4'),
  authMiddleware,
  controler.getUserDeals
)

module.exports = router
