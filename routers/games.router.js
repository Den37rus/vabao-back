const router = require('express').Router()
const { body, param, query } = require('express-validator')
const controler = require('../controlers/Games.controler.js')
const authMiddleware = require('../midlewares/auth.midleware')

router.get('/search', controler.findGame)

router.get('/often', controler.getOftenGames)

module.exports = router
