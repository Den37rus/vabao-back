const router = require('express').Router()
const controler = require('../controlers/InfoData.controler.js')

router.get('/reviews', controler.getReviews)
router.get('/rules', controler.getRules)

module.exports = router
