const router = require('express').Router()
const { body, param, query } = require('express-validator')
const controler = require('../controlers/Auth.controler.js')
const authMiddleware = require('../midlewares/auth.midleware')
router.post(
  '/login',
  body('email').notEmpty().normalizeEmail().isEmail(),
  body('password').isLength({ min: 6, max: 36 }).notEmpty(),
  controler.loginOnPassword
)

router.post(
  '/register',
  body('login').notEmpty().isLength({ min: 4, max: 24 }),
  body('email').notEmpty().isEmail(),
  body('password').notEmpty().isLength({ min: 6, max: 36 }),
  body('rulesConfirm').notEmpty().isBoolean(),
  controler.register
)

router.post('/password-recovery', controler.restorePassword)

router.post(
  '/email-confirm',
  authMiddleware,
  body('email').normalizeEmail().isEmail().not().isEmpty(),
  controler.confirmEmail
)

router.get(
  '/email-confirm/:id',
  authMiddleware,
  param('id').notEmpty().isUUID('4'),
  query('restore-code').notEmpty(),
  controler.confirmEmailSuccess
)

router.get('/refresh', authMiddleware, controler.tokensRefresh)

module.exports = router
