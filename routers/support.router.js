const router = require('express').Router()
const { body, param, query } = require('express-validator')
const controler = require('../controlers/Support.controler.js')

router.post(
  '/',
  body('theme').notEmpty(),
  body('login').notEmpty(),
  body('description').notEmpty(),
  body('captcha').notEmpty(),
  controler.supportRequest
)

module.exports = router
