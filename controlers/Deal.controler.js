const db = require('../db')
const { validationResult } = require('express-validator')

class DealControler {
  async getUserDeals(req, res) {
    const deals = await db.Deal.findAll({
      include: {
        model: db.Order,
        as: 'order_',
        include: [
          {
            model: db.Game,
            as: 'game_',
            attributes: ['title']
          },
          {
            model: db.User,
            as: 'order_owner',
            attributes: ['name', 'login', 'stars', 'last_visit']
          }
        ]
      },
      where: {
        [db.Sequelize.Op.and]: {
          [db.Sequelize.Op.or]: [
            {
              client: { [db.Sequelize.Op.eq]: req.params.id }
            },
            { '$order_.owner$': req.params.id }
          ],
          status: { [db.Sequelize.Op.ne]: 'open' }
        }
      },
      order: [['updatedAt', 'DESC']]
    })

    // console.log(JSON.stringify(deals, null, 4))

    res.status(200).json({ deals })
  }

  async getAllDeals(req, res) {
    res.status(200).json({ ok: true })
  }
}

module.exports = new DealControler()
