const db = require('../db')
const bcrypt = require('bcrypt')
const { TokenFactory } = require('../utils/tokens.js')
const {
  authSuccessMetaResponse,
  clearUserAuthMeta,
  setAuthMeta
} = require('../utils/authHelpers')
const { validationResult } = require('express-validator')

class AuthControler {
  async loginOnPassword(req, res) {
    const { email, password } = req.body
    const validator = validationResult(req)
    if (!validator.isEmpty()) {
      const vaditatorErrorsString =
        'Неверные данные в полях: ' +
        validator
          .array()
          .map(el => el.param)
          .join(' ,')
      return clearUserAuthMeta(res, vaditatorErrorsString)
    }
    try {
      let user = await db.User.findOne({
        where: {
          email: email
        }
      })
      if (!user) {
        return res
          .status(401)
          .json({ error: 'Пользователь не найден или введены неверные данные' })
      }

      if (bcrypt.compareSync(password, user.password)) {
        const { refresh, access } = new TokenFactory({ id: user.id })
        return authSuccessMetaResponse(res, refresh, access, { user: user.id })
      } else {
        return res.status(403).json({ error: 'Ошибка авторизаци' })
      }
    } catch (e) {
      return res.status(400).json({ error: 'Ошибка авторизаци' })
    }
  }

  async register(req, res) {
    const validator = validationResult(req)
    const isRequestValid = validator.isEmpty()
    try {
      if (isRequestValid) {
        const isUserInBase = await db.User.findOne({
          where: {
            [db.sequelize.Sequelize.Op.or]: [
              { login: req.body.login },
              { email: req.body.email }
            ]
          }
        })
        if (isUserInBase == null) {
          const hashedPass = bcrypt.hashSync(req.body.password, 7)
          const newUser = await db.User.create({
            login: req.body.login,
            name: req.body.username,
            password: hashedPass,
            email: req.body.email,
            attributes: {
              exclude: ['password', 'createdAt', 'updatedAt', 'walet']
            }
          })
          const { refresh, access } = new TokenFactory({ id: newUser.id })
          return authSuccessMetaResponse(res, refresh, access, {
            user: newUser.id
          })
        } else {
          return res.status(400).json({
            error: 'Этот email или login уже используются'
          })
        }
      } else {
        const vaditatorErrorsString =
          'Неверные данные в полях: ' +
          validator
            .array()
            .map(el => el.param)
            .join(' ,')
        return clearUserAuthMeta(res, vaditatorErrorsString)
      }
    } catch (e) {
      console.log(e)
    }
  }

  async restorePassword(req, res) {
    try {
      const { email } = req.body
      res.json({ test: 'restore password route' })
    } catch (e) {
      console.log(e)
    }
  }

  async confirmEmail(req, res) {
    try {
      const validator = validationResult(req)
      console.log(validator.errors)
      const isRequestValid = validator.isEmpty()
      if (isRequestValid && req.user) {
        // TO DO   send email with password recovery link
        res.json({ message: 'TO DO   send email with password recovery link' })
      } else {
        res.status(400).json({})
      }
    } catch (e) {
      console.log(e)
    }
  }

  async confirmEmailSuccess(req, res) {
    const validator = validationResult(req)
    console.log(validator.errors)
    const isRequestValid = validator.isEmpty()
    if (isRequestValid) {
      return res.json({ message: 'Email confirmed' })
    }
    res.status(401).json({
      error: validator
        .array()
        .map(el => `Field:${el.param} Error:${el.msg}`)
        .join(' ,')
    })
  }

  async tokensRefresh(req, res) {
    return authSuccessMetaResponse(res, req.refreshToken, req.accessToken, {
      token: req.accessToken
    })
  }
}

module.exports = new AuthControler()
