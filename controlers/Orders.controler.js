const db = require('../db')
const fs = require('fs')
const { saveOrderPhotos, orderImagesPath } = require('../utils/imagesUpload')
const { validationResult } = require('express-validator')

const infoMessage = require('../socket/infoMessages')

class OrderSearch {
  constructor(...args) {}

  searchParamsToQuery(searchParams) {}
}

class OrderControler extends OrderSearch {
  async getOrder(req, res) {
    const validator = validationResult(req)
    const isRequestValid = validator.isEmpty()
    if (isRequestValid) {
      try {
        const order = await db.Order.findOne({
          where: { id: req.params.id },
          include: [
            {
              model: db.User,
              attributes: ['id', 'login', 'stars', 'lastVisit', 'isOnline'],
              as: 'order_owner'
            },
            { model: db.Rewiew, as: 'order_rewiew', include: 'rewiew_author' },
            {
              model: db.Game,
              attributes: [
                'title',
                'searchIndex',
                'searchWeigth',
                'description'
              ],
              as: 'game_'
            },

            {
              model: db.Deal
            }
          ]
        })
        if (!order) {
          return res.status(404).json({ error: 'This order does not exists' })
        }
        return res.status(200).json(order)
      } catch (e) {
        console.log(e)
        res.json({
          error: e
        })
      }
    } else {
      res.json({
        error: validator
          .array()
          .map(el => `Field:${el.param}`)
          .join(' ,')
      })
    }
  }

  async createOrder(req, res) {
    const validator = validationResult(req)
    const isRequestValid = validator.isEmpty()
    if (isRequestValid) {
      try {
        const user = await db.User.findOne({ where: { id: req.user } })
        const order = await user.createOrder(req.body)

        if (!!req.files) {
          const filesKey = Object.keys(req.files)[0]
          const imageFiles = req.files[filesKey]

          fs.mkdir(orderImagesPath(order.id), err => {
            if (err) return console.log(err)
          })

          try {
            const orderImages = saveOrderPhotos(imageFiles, order.id)
            await order.update({ photo: orderImages })
          } catch (e) {
            console.log(e)
          }
        }
      } catch (e) {
        console.log(e)
      }
    } else {
      return res.status(400).json({
        error:
          'Неверные данные в полях: ' +
          validator
            .array()
            .map(el => el.param)
            .join(' ,')
      })
    }
    res.status(200).json({ created: true })
  }

  async buyOrder(req, res) {
    try {
      let deal = await db.Deal.findOne({
        where: { order: req.params.id },
        include: { model: db.Order, as: 'order_' }
      })
      if (req.body.confirmation === true) {
        const dealUpdated = await deal.update({ confirmation: true })
        return res.status(200).json({
          message: 'Заказ подтверждён, средства переведены продавцу',
          deal: dealUpdated
        })
      }

      if (req.body.rewiew && req.user === deal.client) {
        const rewiew = await db.Rewiew.create({
          order: deal.order,
          user: deal.order_.owner,
          author: req.user,
          message: req.body.rewiew.message,
          stars: req.body.rewiew.stars
        })

        const order = await rewiew.getOrder({
          include: [
            {
              model: db.Rewiew,
              as: 'order_rewiew',
              include: {
                model: db.User,
                as: 'rewiew_author',
                attributes: { exclude: ['passord', 'walet'] }
              }
            }
          ]
        })
        return res
          .status(200)
          .json({ message: 'Отзыв добавлен', rewiew: order.order_rewiew })
      }

      const dealUpdated = await deal.update({ client: req.user })
      // console.log(dealUpdated)
      infoMessage(
        { order: dealUpdated.order, status: dealUpdated.status },
        { id: 12345, date: Date.now() }
      )
      return res
        .status(200)
        .json({ message: `Вы оплатили заказ ${deal.order}`, deal: dealUpdated })
    } catch (e) {
      console.log(e)
      res.status(500).json({ error: e })
    }
  }

  async getPriceRange(req, res) {
    const [minMaxPrice] = await db.sequelize.query(
      "SELECT min(price),max(price) FROM Deals as D JOIN Orders as O ON O.id = D.order WHERE D.status = 'open'",
      { type: db.Sequelize.QueryTypes.SELECT }
    )
    //TODO catch nullable values
    return res.status(200).json(minMaxPrice)
  }

  async ordersFiltered(req, res) {
    const searchParams = {
      game: req.query.game || '',
      sortBy: req.query.sortBy || '',
      type: req.query.type || '',
      priceRange: req.query.priceRange || '10,999999',
      page: req.query.page ? req.query.page - 1 : 1
    }

    let ordersSort
    switch (searchParams.sortBy) {
      case 'new':
        ordersSort = ['created_at', 'DESC']
        break

      case 'priceUp':
        ordersSort = ['price', 'ASC']
        break

      case 'priceDown':
        ordersSort = ['price', 'DESC']
        break

      case 'vip':
        ordersSort = [/*'promotion',*/ 'created_at', 'DESC']
        break

      default:
        ordersSort = ['created_at', 'DESC']
        break
    }

    const searchingCategory = searchParams.type.split(',').some(el => el === '')
      ? ['account', 'item', 'service', 'other']
      : searchParams.type.split(',')

    const itemsCount = 20
    const resultPage =
      req.query.page && req.query.page > 0
        ? (req.query.page - 1) * itemsCount
        : 0

    try {
      const { rows: result, count } = await db.Order.findAndCountAll({
        where: {
          category: {
            [db.Sequelize.Op.in]: searchingCategory
          },
          price: {
            [db.Sequelize.Op.between]: searchParams.priceRange.split(',')
          }
        },
        include: [
          {
            model: db.Deal,
            required: true,
            where: {
              status: 'open'
            },
            attributes: ['status']
          },
          {
            model: db.Game,
            as: 'game_',
            attributes: ['title'],
            where: {
              title: {
                [db.Sequelize.Op.iLike]: `%${searchParams.game}%`
              }
            }
          },
          {
            model: db.User,
            as: 'order_owner',
            attributes: ['login']
          }
        ],
        limit: itemsCount,
        offset: resultPage, // TODO pagination
        order: [ordersSort]
      })

      return res.status(200).json({ orders: result, count })
    } catch (e) {
      console.log(e)
      res.status(500).json({ error: 'Server error' })
    }
  }
}

module.exports = new OrderControler()
