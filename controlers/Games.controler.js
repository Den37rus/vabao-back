const db = require('../db')
const { validationResult } = require('express-validator')

class UserControler {
  async findGame(req, res) {
    if (!req.query.game) {
      return res.status(200).json([])
    }
    const games = await db.Game.findAll({
      where: {
        title: { [db.sequelize.Sequelize.Op.iLike]: `%${req.query.game}%` }
      },
      order: [['search_weigth', 'DESC']],
      limit: 8
    })

    res.status(200).json(games)
  }

  async getOftenGames(req, res) {
    const oftenGames = await db.Game.findAll({
      attributes: ['id', 'title', 'description'],
      order: [['search_weigth', 'DESC']],
      limit: 8
    })
    res.status(200).json(oftenGames)
  }
}

module.exports = new UserControler()
