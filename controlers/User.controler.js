const db = require('../db')
const config = require('../config')
const { dropUserPhoto, saveUserPhoto } = require('../utils/imagesUpload')
const { clearAuthMeta } = require('../utils/authHelpers')
const { validationResult } = require('express-validator')

class UserControler {
  async getUser(req, res) {
    const validator = validationResult(req)
    const isRequestValid = validator.isEmpty()
    try {
      if (isRequestValid) {
        let user = await db.User.findOne({
          where: { id: req.params.id },
          include: [
            {
              model: db.Order,
              as: 'orders',
              include: [
                {
                  model: db.Deal,
                  attributes: {
                    exclude: ['order', 'outgoingTransfer', 'incomingTransfer']
                  }
                },
                {
                  model: db.Game,
                  as: 'game_',
                  attributes: { include: ['title'] }
                },
                {
                  model: db.User,
                  as: 'order_owner',
                  attributes: ['login', 'last_visit', 'isOnline']
                }
              ]
            },
            {
              model: db.Rewiew,
              as: 'user_rewiews',
              include: {
                model: db.User,
                as: 'rewiew_author',
                attributes: ['login', 'name']
              }
            }
          ],

          attributes: {
            exclude: ['email', 'password', 'walet', 'updatedAt']
          },
          order: [
            ['orders', 'updatedAt', 'DESC'],
            ['user_rewiews', 'updatedAt', 'DESC']
          ]
        })
        res.json(user)
      }
    } catch (e) {
      console.log(e)
      res.status(400).json({ error: e })
    }
  }

  async getMyTransactions(req, res) {
    const validator = validationResult(req)
    if (validator.isEmpty() && req.user) {
      try {
        const user = await db.User.findOne({
          where: { id: req.user },
          attributes: {
            exclude: ['password', 'createdAt', 'updatedAt', 'walet']
          },
          include: [
            {
              model: db.Walet,
              as: 'user_walet',
              attributes: { exclude: ['id', 'createdAt', 'updatedAt'] },
              include: {
                model: db.Transaction,
                as: 'transactions',
                attributes: {
                  exclude: [
                    'id',
                    'walet_id',
                    'acceptedAt',
                    'rejectedAt',
                    'moderator'
                  ]
                },
                include: [
                  {
                    model: db.Deal
                  }
                ]
              }
            }
          ],
          order: [['user_walet', 'transactions', 'createdAt', 'ASC']]
        })

        if (!user) {
          return clearAuthMeta(res, 'Авторизуйтесь')
        }

        return res.status(200).json({
          transactions: user.user_walet.transactions,
          balance: user.user_walet.balance
        })
      } catch (e) {
        console.log(e)
        return res.status(200).json({ error: e.message })
      }
    }
    return res.status(401).json({ error: 'invalid user' })
  }

  async testPayment(req, res) {
    if (req.body.sum < 10) {
      return res.status(200).json({ error: 'Минимальная сумма 10 руб.' })
    }
    const user = await db.User.findOne({
      where: { id: req.user }
    })
    const walet = await user.getUser_walet()

    await walet.createTransaction({
      sum: req.body.sum,
      type: 'payment',
      accepted: true
    })

    const transactions = await walet.getTransactions({
      attributes: {
        exclude: ['walet_id']
      }
    })
    const { balance: updatedBalance } = await user.getUser_walet()
    res.status(200).json({ transactions, balance: updatedBalance })
  }

  async updateUserProfile(req, res) {
    if (req.files) {
      const uploadedFilesKey = Object.keys(req.files)[0]
      let filePhoto = req.files[uploadedFilesKey]
      if (Array.isArray(filePhoto)) filePhoto = filePhoto[0]
      try {
        const result = saveUserPhoto(filePhoto.name, filePhoto.data)
      } catch (e) {
        console.log(e)
        return res.status(500).json({ error: 'Ошибка сервера' })
      }
    }
    try {
      const user = await db.User.findOne({ where: { id: req.user } })
      if (req.body.deletePhoto === 'true') {
        dropUserPhoto(req.user)
      }

      await user.update({
        name: req.body.name || user.name,
        description: req.body.description || user.description
      })
      return res.status(200).json(user)
    } catch (e) {
      console.log(e)
      return res.status(400).json({ error: 'Ошибка при изменении профиля' })
    }
  }
}

module.exports = new UserControler()
