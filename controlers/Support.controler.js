const db = require('../db')
const config = require('../config')
const { validationResult } = require('express-validator')
const axios = require('axios')

class SupportControler {
  async supportRequest(req, res) {
    const validator = validationResult(req)
    if (!validator.isEmpty()) {
      return res.status(200).json({ error: 'Неправильно заполнена форма' })
    }
    const { data: captchaResult } = await axios.get(
      'https://www.google.com/recaptcha/api/siteverify',
      {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        params: { secret: config.CAPTCHA_SECRET, response: req.body.captcha }
      }
    )

    if (!captchaResult.success) {
      return res.status(200).json({ error: 'Неверная капча' })
    }

    /* TODO create table in database for support requests
    /       set files upload handler
    /       
    */
    res.end()
  }
}

module.exports = new SupportControler()
