const db = require('../db')
const config = require('../config')

class InfoDataControler {
  async getReviews(req, res) {
    res.json(require('../docs/UserReviews.json'))
  }

  async getRules(req, res) {
    res.json(require('../docs/rulesData.json'))
  }
}

module.exports = new InfoDataControler()
