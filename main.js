const config = require('./config.js')
const db = require('./db')

const { server } = require('./socket/socket')

db.sync()
  .then(() => {
    server.listen(config.PORT, err => {
      if (err) throw new Error(err.message)
      console.log(`Server start on http://localhost:${config.PORT}`)
    })
  })
  .catch(e => console.log(e.message))

// const fakeGames = [
//   { title: 'Sims 3', description: '' },
//   { title: 'RAID Shadow Legends', description: '' },
//   { title: 'Caliber', description: 'Калибр' },
//   { title: 'The Last Spell', description: '' },
//   { title: 'Vampire Survivors', description: '' },
//   { title: "Baldur's Gate 3", description: '' },
//   { title: 'War Thunder', description: '' },
//   { title: 'Fortnite Battle Royale', description: '' },
//   { title: 'World of Warships', description: '' },
//   { title: 'Crossout', description: '' },
//   { title: 'Star Conflict Journey', description: '' },
//   { title: 'League of Legends', description: '' },
//   { title: 'Lineage II', description: '' },
//   { title: 'Aion', description: '' },
//   { title: 'Black Desert', description: '' },
//   { title: 'Blade & Soul', description: '' },
//   { title: 'Lost Ark', description: '' },
//   { title: 'ArcheAge', description: '' },
//   { title: 'World of Warcraft', description: '' },
//   { title: 'Overwatch', description: '' },
//   { title: 'Deep Rock Galactic', description: '' },
//   { title: 'Battlefield 3', description: '' },
//   { title: 'Counter-Strike: Global Offensive', description: '' },
//   { title: 'Hearthstone', description: '' },
//   { title: 'Pagan Online', description: '' },
//   { title: 'The Ascent', description: '' },
//   { title: 'The Crew 2', description: '' },
//   { title: 'PUBG Lite PC', description: '' },
//   {
//     title: 'Warface',
//     description: 'Chiters game'
//   },
//   {
//     title: 'World of Tanks',
//     description: 'Stuped game'
//   },
//   {
//     title: 'WarPath',
//     description: 'Mobile game'
//   }
// ]

// db.Game.bulkCreate(fakeGames)
