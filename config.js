require('dotenv').config({ path: './.database.env' }).parsed
const path = require('path')

const ROOT_PATH = path.normalize(__dirname)
const imagesPath = path.join(ROOT_PATH, 'images')

const orderImgFolderName = 'order'
const userImgFolderName = 'user'
const orderImgFolder = path.join(imagesPath, orderImgFolderName)
const userImgFolder = path.join(imagesPath, userImgFolderName)
const imagesExtension = '.webp'

const accessSecret = 'a9s8dua9f*@Y!E&HUiafnjsd9guerig'
const refreshSecret =
  '9Cuoasfjewn8(AFU(*Q@Dnlaafnu32ufim209u(H&*r2yi1hkfj0dgjpeherphoms;u*(!&~*YURJBFG'
const CAPTCHA_SECRET = '6LfBycobAAAAAHMlkrLtfE-lGa9oka87XpZBEPRP'

const SOCKET_PATH = '/socket/v1/'

module.exports = {
  PORT: 3000,
  accessSecret,
  refreshSecret,
  CAPTCHA_SECRET,
  ROOT_PATH,
  SOCKET_PATH,
  imagesPath,
  orderImgFolder,
  userImgFolder,
  orderImgFolderName,
  userImgFolderName,
  imagesExtension
}
