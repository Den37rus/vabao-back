const fs = require('fs')
const path = require('path')
const cookieParser = require('cookie-parser')
const express = require('express')
const app = express()
const cors = require('cors')
const config = require('./config')

const fileUpload = require('express-fileupload')
const AuthRouter = require('./routers/auth.router.js')
const UserRouter = require('./routers/user.router')
const OrdersRouter = require('./routers/orders.router')
const DealsRouter = require('./routers/deals.router')
const GamesRouter = require('./routers/games.router')
const SupportRouter = require('./routers/support.router')
const InfoData = require('./routers/infoData.router')

app.use(cors())
app.use(cookieParser())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(fileUpload())
app.disable('x-powered-by')

const rules = require('./docs/rulesData.json')
app.get('/docs/rulesData', (req, res) => {
  console.log('req')
  res.status(200).json(rules)
})

app.use('/images/order', express.static(config.orderImgFolder))

app.use(
  '/images/user',
  (req, res, next) => {
    const filePath = path.join(config.userImgFolder, req.path)
    if (!fs.existsSync(filePath)) {
      return res.sendFile(path.join(config.userImgFolder, 'default.webp'))
    }
    next()
  },
  express.static(config.userImgFolder, {
    index: false,
    fallthrough: false
  })
)

app.use('/auth', AuthRouter)
app.use('/api/user', UserRouter)
app.use('/api/order', OrdersRouter)
app.use('/api/deals', DealsRouter)
app.use('/api/games', GamesRouter)
app.use('/api/support', SupportRouter)
app.use('/api/info', InfoData)
app.use((error, request, response, next) => {
  response.status(error.status || 500).json({
    error: error.message
  })
})

module.exports = app
