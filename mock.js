const transactions = [
  {
    id: 1,
    date: 1625239930000,
    type: '#2',
    status: false,
    amount: 44,
  },
  {
    id: 2,
    date: 1632670330000,
    type: '#1',
    status: true,
    amount: 44,
  },
  {
    id: 4,
    date: 1632756730000,
    type: 'Вывод средств',
    status: false,
    amount: 44,
  },
  {
    id: 12,
    date: 1634997515164,
    type: 'Пополнение баланса',
    status: true,
    amount: 44,
  },
  {
    id: 3,
    date: 1633188730000,
    type: '#3',
    status: true,
    amount: 44,
  },
  {
    id: 5,
    date: 1634052730000,
    type: '#4',
    status: true,
    amount: 44,
  },
  {
    id: 13,
    date: 1632324730000,
    type: 'Вывод средств',
    status: true,
    amount: 44,
  },
  {
    id: 15,
    date: 1634916730000,
    type: 'Вывод средств',
    status: false,
    amount: 44,
  },
];

const orders = [
  {
    id: 1,
    type: '',
    creatorId: 3,
    clientId: 654,
    images: [],
    status: 'open',
    game: 'Brawl Stars',
    price: 150,
    category: 'Предмет',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.`,
  },
  {
    id: 2,
    creatorId: 2,
    clientId: 654,
    images: [],
    status: 'inProgress',
    game: 'World of Tanks',
    price: 50,
    category: 'Услуга',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.`,
  },
  {
    id: 3,
    creatorId: 1,
    clientId: 654,
    images: [],
    status: 'open',
    game: 'Sims',
    price: 250,
    category: 'Прочее',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.`,
  },
  {
    id: 4,
    creatorId: 2,
    clientId: 654,
    images: [],
    status: 'close',
    game: 'Warface',
    price: 350,
    category: 'Аккаунты',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.`,
  },
];

const users = [
  { id: 1, login: 'Вася', foto: null, stars: 4 },
  {
    id: 2,
    login: 'Petya',
    foto: null,
    stars: 5,
  },
  {
    id: 3,
    login: 'Fedya',
    foto: null,
    stars: 2,
  },
  {
    id: 4,
    login: 'Den',
    foto: null,
    stars: 2,
  },
];

module.exports = {
  transactions,
  orders,
  users,
};
