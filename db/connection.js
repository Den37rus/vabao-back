const Database = require('./Database')

const db = new Database({
  DB_HOST: process.env.DB_HOST ?? console.error('bad host'),
  DB_NAME: process.env.DB_NAME ?? console.error('bad name'),
  DB_USER: process.env.DB_USER ?? console.error('bad user'),
  DB_PASSWORD: process.env.DB_PASSWORD ?? console.error('bad pass')
  // LOGGER: err => console.log(err)
})

db.requireModels(
  './Models/User.model',
  './Models/Order.model',
  './Models/Game.model',
  './Models/Deal.model',
  './Models/Walet.model',
  './Models/Transaction.model',
  './Models/Rewiew.model',
  './Models/ChatRoom.model',
  './Models/ChatMessage.model'
)

db.User.belongsTo(db.Walet, { foreignKey: 'walet', as: 'user_walet' })
db.User.hasMany(db.Order, { foreignKey: 'owner', as: 'orders' })
db.User.hasMany(db.Deal, { foreignKey: 'client', as: 'deals' })
db.User.hasMany(db.ChatRoom, { foreignKey: 'owner' })
db.User.hasMany(db.ChatRoom, { foreignKey: 'user' })
db.User.hasMany(db.ChatMessage, { foreignKey: 'user' })
db.User.hasMany(db.Rewiew, { foreignKey: 'user', as: 'user_rewiews' })
db.User.hasMany(db.Rewiew, {
  foreignKey: 'author',
  as: 'rewiew_author'
})

db.Rewiew.belongsTo(db.User, { foreignKey: 'user', as: 'user_' })
db.Rewiew.belongsTo(db.User, { foreignKey: 'author', as: 'rewiew_author' })
db.Rewiew.belongsTo(db.Order, { foreignKey: 'order' })

db.Walet.hasMany(db.Transaction, { foreignKey: 'walet_id', as: 'transactions' })

db.Transaction.belongsTo(db.Walet, { foreignKey: 'walet_id', as: 'walet' })
db.Transaction.belongsTo(db.Deal)

db.Order.hasOne(db.Deal, { foreignKey: 'order' })
db.Order.belongsTo(db.User, { foreignKey: 'owner', as: 'order_owner' })
db.Order.belongsTo(db.Game, { foreignKey: 'game', as: 'game_' })
db.Order.hasMany(db.ChatRoom, { foreignKey: 'order' })
db.Order.hasOne(db.Rewiew, { foreignKey: 'order', as: 'order_rewiew' })

db.Deal.belongsTo(db.User, { foreignKey: 'client', as: 'client_' })
db.Deal.belongsTo(db.Order, { foreignKey: 'order', as: 'order_' })
db.Deal.hasMany(db.Transaction)

db.ChatRoom.hasMany(db.ChatMessage, { foreignKey: 'room' })

const userStarsCounter = async rew => {
  const user = await rew.getUser_({
    include: 'user_rewiews'
  })

  if (user.user_rewiews.length === 0) return
  const stars =
    user.user_rewiews.reduce((acc, el) => {
      return acc + Number(el.stars)
    }, 5) /
    (user.user_rewiews.length + 1)
  await user.update({ stars: stars.toFixed(2) })
}

db.Rewiew.afterCreate(userStarsCounter)
db.Rewiew.afterDestroy(userStarsCounter)
db.Rewiew.beforeCreate(async rewiew => {
  const order = await rewiew.getOrder()
  const oldRewiew = await order.getOrder_rewiew()
  if (oldRewiew) {
    await oldRewiew.destroy()
  }
})

db.User.beforeCreate(async user => {
  user.createUser_walet(user)
})

db.Order.afterCreate(async order => {
  order.createDeal(order)
})

db.Deal.afterUpdate(async deal => {
  const { price } = await deal.getOrder_()

  if (deal.client && deal.status === 'open') {
    await deal.update({ status: 'progress' })
    const client = await deal.getClient_()
    const walet = await client.getUser_walet()
    const transfer = await walet.createTransaction({
      type: 'withdraw',
      sum: price,
      reason: 'order',
      accepted: true
    })

    await deal.addTransaction(transfer)
  }

  if (deal.confirmation === true && deal.status === 'progress') {
    await deal.update({ status: 'close' })
    const { order_owner: owner } = await deal.getOrder_({
      include: {
        model: db.User,
        as: 'order_owner',
        include: 'user_walet'
      }
    })
    const transfer = await owner.user_walet.createTransaction({
      type: 'payment',
      sum: price,
      reason: 'order',
      accepted: true
    })
    await deal.addTransaction(transfer)
  }
})

db.Transaction.afterCreate(async i => {
  const walet = await i.getWalet()
  if (i.accepted) {
    if (i.type === 'payment') {
      await walet.update({
        balance: Number(walet.balance) + i.sum,
        acceptedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }

    if (i.type === 'withdraw') {
      await walet.update({
        balance: Number(walet.balance) - i.sum,
        acceptedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }
  }

  if (i.rejected) {
    if (i.type === 'payment') {
      await walet.update({
        balance: Number(walet.balance) - i.sum,
        rejectedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }

    if (i.type === 'withdraw') {
      await walet.update({
        balance: Number(walet.balance) + i.sum,
        rejectedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }
  }
})

db.Transaction.afterUpdate(async i => {
  const walet = await i.getWalet()
  if (i.accepted) {
    if (i.type === 'payment') {
      await walet.update({
        balance: Number(walet.balance) + i.sum,
        acceptedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }

    if (i.type === 'withdraw') {
      if (walet.balance < i.sum) {
        return
      }
      await walet.update({
        balance: Number(walet.balance) - i.sum,
        acceptedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }
  }

  if (i.rejected) {
    if (i.type === 'payment') {
      await walet.update({
        balance: Number(walet.balance) - i.sum,
        rejectedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }

    if (i.type === 'withdraw') {
      await walet.update({
        balance: Number(walet.balance) + i.sum,
        rejectedAt: db.Sequelize.literal('CURRENT_TIMESTAMP')
      })
    }
  }
})

module.exports = db
