module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const ChatRoom = sequelize.define(
    'ChatRoom',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      order: {
        type: DataTypes.INTEGER,
        required: true
      },
      owner: {
        type: DataTypes.UUID,
        defaultValue: null,
        required: true
      },
      user: {
        type: DataTypes.UUID,
        defaultValue: null,
        required: true
      },
      closed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      }
    },
    {
      sequelize,
      validate: {
        rejectEmptyMessage() {
          if (!this.message && !this.photo) {
            throw new Error('Message not must be empty')
          }
        }
      }
    }
  )

  return ChatRoom
}
