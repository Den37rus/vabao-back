module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const Walet = sequelize.define('Walet', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    balance: {
      type: DataTypes.REAL,
      defaultValue: 0,
      validator: {
        min: 0,
        msg: 'Low balance'
      }
    }
  })

  return Walet
}
