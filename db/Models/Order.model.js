module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const Order = sequelize.define('Order', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    photo: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: true
    },
    category: {
      type: DataTypes.ENUM('account', 'item', 'service', 'other'),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    owner: {
      type: DataTypes.UUID,
      allowNull: false
    },
    price: {
      type: DataTypes.REAL,
      allowNull: false,
      validator: {
        min: 1
      }
    },
    game: {
      type: DataTypes.UUID,
      allowNull: false
    },
    moderate: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    moderator: {
      type: DataTypes.UUID,
      required: true
    }
  })

  return Order
}
