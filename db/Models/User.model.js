module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const User = sequelize.define('User', {
    id: {
      defaultValue: DataTypes.UUIDV4,
      type: DataTypes.UUID,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    login: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    isEmailConfirm: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isEmailNoticeOn: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    stars: {
      type: DataTypes.DECIMAL,
      defaultValue: 5.0
    },
    isOnline: {
      type: DataTypes.BOOLEAN
    },
    lastVisit: {
      type: DataTypes.DATE,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    }
  })

  return User
}
