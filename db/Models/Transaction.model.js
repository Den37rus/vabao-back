module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const Transaction = sequelize.define('Transaction', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    type: {
      type: DataTypes.ENUM('payment', 'withdraw'),
      allowNull: false
    },
    sum: {
      type: DataTypes.REAL,
      validator: {
        min: 0
      }
    },
    accepted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    rejected: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    acceptedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    rejectedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    moderator: {
      type: DataTypes.UUID,
      required: true
    },
    reason: {
      type: DataTypes.STRING
    }
  })

  return Transaction
}
