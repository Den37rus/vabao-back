module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const Deal = sequelize.define('Deal', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    client: {
      type: DataTypes.UUID,
      defaultValue: null,
      allowNull: true
    },
    confirmation: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    status: {
      type: DataTypes.ENUM('open', 'close', 'inProgress')
    }
    // description: {
    //   type: DataTypes.TEXT
    // }
  })

  return Deal
}
