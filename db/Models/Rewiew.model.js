module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const Rewiew = sequelize.define('Rewiew', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    user: {
      type: DataTypes.UUID,
      allowNull: false
    },
    author: {
      type: DataTypes.UUID,
      allowNull: false
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    stars: {
      type: DataTypes.INTEGER,
      validator: {
        min: 1,
        max: 5
      },
      allowNull: false
    }
  })

  return Rewiew
}
