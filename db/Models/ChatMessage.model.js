module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const ChatMessage = sequelize.define(
    'ChatMessage',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      room: {
        type: DataTypes.UUID,
        required: true
      },
      user: {
        type: DataTypes.UUID,
        defaultValue: null,
        required: true
      },
      message: {
        type: DataTypes.STRING,
        allowNull: true
      },
      photo: {
        type: DataTypes.TEXT,
        defaultValue: null,
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('send', 'delivered', 'read'),
        defaultValue: 'send'
      }
    },
    {
      sequelize,
      validate: {
        rejectEmptyMessage() {
          if (!this.message && !this.photo) {
            throw new Error('Message not must be empty')
          }
        }
      }
    }
  )

  return ChatMessage
}
