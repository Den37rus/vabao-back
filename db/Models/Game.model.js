module.exports = sequelize => {
  const { DataTypes } = sequelize.Sequelize
  const Game = sequelize.define('Game', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    reaiting: {
      type: DataTypes.DECIMAL,
      defaultValue: 0.0
    },
    searchIndex: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    searchWeigth: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  })

  return Game
}
