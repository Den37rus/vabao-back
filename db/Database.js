const { Sequelize } = require('sequelize')
const path = require('path')
const fs = require('fs')

class Database {
  constructor(connectionOptions) {
    this.sequelize = new Sequelize(
      connectionOptions.DB_NAME,
      connectionOptions.DB_USER,
      connectionOptions.DB_PASSWORD,
      {
        host: connectionOptions.DB_HOST ?? 'localhost',
        dialect: connectionOptions.DB_DIALECT ?? 'postgres',
        logging:
          process.env.MODE === 'production'
            ? false
            : connectionOptions.LOGGER ?? false,
        define: {
          underscored: true,
          charset: 'utf8',
          timestamps: true
        }
      }
    )
    this.Sequelize = this.sequelize.Sequelize
  }

  async __migrateForce() {
    if (process.env.MODE === 'production') {
      throw new Error(
        'In prod mode db.sync not be called.You need start Sequelize-CLI:migrate'
      )
    }
    await this.sequelize.sync({ force: true })
  }

  async sync() {
    // if (process.env.MODE === 'production') {
    //   throw new Error(
    //     'In prod mode db.sync not be called.You need start Sequelize-CLI:migrate'
    //   )
    // }
    return this.sequelize.sync()
  }

  requireModels(...modelPath) {
    if (!modelPath.length) {
      throw new Error('Arguments is required and must be path like string')
    }
    modelPath.forEach(el => {
      if (typeof el !== 'string')
        throw new Error('Arguments is required and must be path like string')
      if (!fs.existsSync(__dirname, el))
        throw new Error(`Model on path: ${el} not exists`)
      let { name } = path.parse(el)
      const [modelName, _] = name.split('.')
      this[modelName] = require(el)(this.sequelize)
    })
  }
}

module.exports = Database
